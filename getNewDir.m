%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to obtaine new exploration direction

% INPUT:
% iter: number of iteration (of the exploration)
% attractive_point: coordinate of the attractive point in unitary space
% blocked point: out-of-wk points in physical space
% params: structure with simulation parameters
% drawnowopts: options for computation plotting

% OUTPUT:
% dir = exploration direction in unitary space

function dir_new = getNewDir(iter,attractive_point,blocked_points,pstart,params,drawnowopts)

tau = params.tau;

%% Transformation to unitary domain
p_u = physical2unitary(pstart,params);
a_u = attractive_point';

%% ALLOWED
dir_a = (a_u-p_u)/norm(a_u-p_u);
%% BLOCKED
if numel(blocked_points)>0
    n = 1;
    dir_b = zeros(2,1);
    n_block = numel(blocked_points(:,1));
    while n<=n_block
        b_u = physical2unitary(blocked_points(n,:)',params);
        dir_b_n = (b_u-p_u)/norm(b_u-p_u);
        c_f = 1-norm(b_u-p_u);
        dir_b_n = c_f*dir_b_n;
        dir_b = dir_b + dir_b_n;
        n = n + 1;
    end
    dir_b = dir_b/norm(dir_b);
else
    dir_b = zeros(2,1);
end
%% NEW DIRECTION
c_b1 = exp(-iter/tau);
c_a1 = (1-exp(-iter/tau));
d_a = c_a1*dir_a;
d_b = - c_b1*dir_b;
dir_u = d_a+d_b;
dir = dir_u/norm(dir_u);
dir_new = convert_direction(dir,params);

if drawnowopts == 1

        h1 = quiver(pstart(1),pstart(2),d_a(1),d_a(2),'Color','blue','LineWidth',2);
        h2 = quiver(pstart(1),pstart(2),d_b(1),d_b(2),'Color','red','LineWidth',2);
        h3 = quiver(pstart(1),pstart(2),dir_u(1),dir_u(2),'Color','green','LineWidth',2);
        pause(0.1)
        delete(h1)
        delete(h2)
        delete(h3)
end


end