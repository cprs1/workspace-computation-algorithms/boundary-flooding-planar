%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to get attractive points in WK

% INPUT:
% WK: array of workspace points
% p_fail: point where computation failed
% p_start: point where the computation started
% fail_idx: index that identifies p_fail ok WK
% params: structure with simulation parameters

% OUTPUT:
% ind_neigh_todo: indices of points to be computed
% idn2: neighbors of p_fail

function [id_neigh_todo,idn2] = getFailBox(WK,p_fail,pstart,fail_idx,params)

idn2 = getNeighbors(WK,p_fail,params);
idn2 = idn2((idn2~=fail_idx)==1);

dist = (abs(WK(idn2,2)-pstart(1)).^2+abs(WK(idn2,3)-pstart(2)).^2).^(0.5);
[~,I] = sort(dist);
idn2_order = idn2(I);

id_neigh_todo = (WK(idn2_order,4)==0);
id_neigh_todo = idn2_order(id_neigh_todo);

end