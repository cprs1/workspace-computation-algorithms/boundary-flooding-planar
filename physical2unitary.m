%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to convert points in physical space to points in unitary space
% INPUT:
% pf: point in physical space
% params: structure with simulation parameters

% OUTPUT:
% pu: point in unitary space

function pu = physical2unitary(pf,params)

boxsize = params.boxsize;

xmin = boxsize(1);
xmax = boxsize(2);
ymin = boxsize(3);
ymax = boxsize(4);

pu = [(pf(1)-xmin)/(xmax-xmin);
      (pf(2)-ymin)/(ymax-ymin);];
end