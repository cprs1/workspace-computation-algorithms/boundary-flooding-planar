%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to convert exploration direction from unitary to physical.
% each component is reported in the physical domain and then approximated
% to the closer point according to the stepsize.

% INPUT:
% dir_it: direction in unitary space
% params: structure with simulation parameters

% OUTPUT:
% dir_new = direction in the physical space

function dir_new = convert_direction(dir_it,params)
boxsize = params.boxsize;
stepsize_x = params.stepsize_x;
stepsize_y = params.stepsize_y;

dir_new = [dir_it(1)*(boxsize(2)-boxsize(1))*stepsize_x;
           dir_it(2)*(boxsize(4)-boxsize(3))*stepsize_y;];

if abs(dir_new(1))>stepsize_x
    dir_new = stepsize_x*dir_new/abs(dir_new(1));
end

if abs(dir_new(2))>stepsize_x
    dir_new = stepsize_x*dir_new/abs(dir_new(2));
    
end

end