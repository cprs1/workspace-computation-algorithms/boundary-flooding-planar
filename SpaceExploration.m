function [WK,guesses,wk_todo,blocked_points,physical_attractive] = SpaceExploration(WK,guesses,params,fcn,start_to_go_idx,attractive_point,blocked_points,drawnowopts,nr)
    %% INITIALIZATION
    wk_todo = [];
    y0 = guesses(:,start_to_go_idx);
    pstart = params.pstart;
    exitflag = 1;
    iter = 1;
    a_f = unitary2physical(attractive_point,params);
    if drawnowopts == 1
        plot(a_f(1),a_f(2),'ro')
        drawnow
    end
    physical_attractive(nr,:) = a_f;
    
    %% EXPLORE: go up until border-condition occurs
    while exitflag==1
        % compute new direction and identify new point
        dir_new = getNewDir(iter,attractive_point,blocked_points,pstart,params,drawnowopts);
        pdes = pstart + dir_new;
        neighbors_idx = getNeighbors(WK,pdes,params);
        neighbors_todo_idx = neighbors_idx(WK(neighbors_idx,5)==0);
        if numel(neighbors_todo_idx)==0
            exitflag = 0 ;
        else
            % solve igsp
            point_to_go_idx = neighbors_todo_idx(1);
            pend = [WK(point_to_go_idx,2);WK(point_to_go_idx,3)];
            [y,solve_flag,jac] = solveIGSP(params,fcn,pend,y0);
            
            % save results
            norminv = norm(inv(jac),Inf);
            WK(point_to_go_idx,6) = norminv;
            [exitflag,exitidx] = getOutCondition(y,jac,pend,solve_flag,norminv,params,fcn);
            % update
            iter = iter+1;
            pstart = pdes;           
            WK(point_to_go_idx,4) = exitidx;
            if exitflag == 1
                WK(point_to_go_idx,5) = 0;
                guesses(:,point_to_go_idx) = y;
                y0 = y;
                old_points_to_go_idx = point_to_go_idx;
                if drawnowopts == 1
                    plot(pend(1),pend(2),'c.')
                    drawnow
                end
            end
        end
    end
    
    %% EXPLORATION COMPLETED: save results
    blocked_points = [blocked_points;pend'];        
    if WK(point_to_go_idx,5) == 0 && exitidx~=0 
        WK(point_to_go_idx,5) = nr;
        WK(point_to_go_idx,5) = nr;
        wk_todo = [point_to_go_idx,old_points_to_go_idx];
        if drawnowopts == 1
            plot(pend(1),pend(2),'ro','LineWidth',2)
            plot(WK(old_points_to_go_idx,2),WK(old_points_to_go_idx,3),'bo','LineWidth',2)
            drawnow
        end
   end  
end