%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% boundary flooding algorithm function

% INPUTS:
% fcn = structure with objective functions
% params = structure with simulation parameters
% drawnowopts = option for plotting

% OUTPUTS:
% WK: array with workspace data
% outstruct = structure with results

function [WK,outstruct] = FCN_BoundaryFloodingPlanarWK(fcn,params,drawnowopts)
tic;

%% HOW WK IS ORDERED:
% 1: point idx
% 2: x coordinate
% 3: y coordinate
% 4: idx to identify kind of point  
% 5: n° of search direction employed
% 6: inverse cond of Jac at this point


% wk(:,4) =
% 1: point in the wk
% 2: solver fails
% 3: norm inv too high
% 4: type 1 singu
% 5: type 2 singu
% 6: change in stab
% 7: mech constr
% 8: rad vector

% %% COLLECT INPUTS
% y0 = params.y0;


[WK,guesses,start_to_go_idx] = initialization(params,fcn);

%% RADIATING VECTORS

attractive_points = getAttractiveCooord(params);
blocked_points = [];
nr = 1;
n_rad = params.n_rad; % number of radiating vectors equaly spaced in 0,2\pi

while nr <= n_rad  
    attractive_point = attractive_points(nr,:);
    [WK,guesses,wk_todo,blocked_points,physical_attractive] = SpaceExploration(WK,guesses,params,fcn,start_to_go_idx,attractive_point,blocked_points,drawnowopts,nr);
    [WK,guesses] = BorderReconstruction(wk_todo,WK,guesses,params,fcn,nr,drawnowopts);
    nr = nr + 1;
end

%% DATA
idx_computed = WK(WK(:,6)~=0,1);
WK = WK(idx_computed ,:);
time = toc;
outstruct.time = time/60;
outstruct.attractivepoints = physical_attractive;

end