function h = Plot3PFR(pos1,pos2,pos3,pP,thP,geometry,L,actlim)
basepoints = geometry.basepoints;
platpoints = geometry.platpoints;
baseangles = geometry.baseangles;

A1 = basepoints(:,1);
A2 = basepoints(:,2);
A3 = basepoints(:,3);
B1 = platpoints(:,1);
B2 = platpoints(:,2);
B3 = platpoints(:,3);

th1 = baseangles(1);
th2 = baseangles(2);
th3 = baseangles(3);

B1f = Rz(thP)*B1+pP; %wrt fixed frame
B2f = Rz(thP)*B2+pP;
B3f = Rz(thP)*B3+pP;

% figure()
plot(A1(1),A1(2),'bo','LineWidth',1) % base points
hold on
plot(A2(1),A2(2),'bo','LineWidth',1)
plot(A3(1),A3(2),'bo','LineWidth',1)
grid on
h1 = plot(B1f(1),B1f(2),'ko','LineWidth',1); % platform points
h2 = plot(B2f(1),B2f(2),'ko','LineWidth',1);
h3 = plot(B3f(1),B3f(2),'ko','LineWidth',1);
h4 = line([B1f(1) B2f(1)],[B1f(2) B2f(2)],'Color','k','LineWidth',1); % platform
h5 = line([B2f(1) B3f(1)],[B2f(2) B3f(2)],'Color','k','LineWidth',1);
h6 = line([B3f(1) B1f(1)],[B3f(2) B1f(2)],'Color','k','LineWidth',1);

h7 = plot(pos1(1,:),pos1(2,:),'k','LineWidth',1); % flexible beams
h8 = plot(pos2(1,:),pos2(2,:),'k','LineWidth',1);
h9 = plot(pos3(1,:),pos3(2,:),'k','LineWidth',1);

x_ax = [1;0]*0.2*L; % fixed frame
y_ax = [0;1]*0.2*L;

x_1x = (Rz(th1)*[1;0])*0.2*L; % act 1 frame
y_1x = (Rz(th1)*[0;1])*0.2*L;
x_2x = (Rz(th2)*[1;0])*0.2*L; % act 2 frame
y_2x = (Rz(th2)*[0;1])*0.2*L;
x_3x = (Rz(th3)*[1;0])*0.2*L; % act 3 frame
y_3x = (Rz(th3)*[0;1])*0.2*L;

h10 = quiver(A1(1),A1(2),x_1x(1),x_1x(2),'m','LineWidth',1) ;
h11 = quiver(A1(1),A1(2),y_1x(1),y_1x(2),'r','LineWidth',1);
h12 = quiver(A2(1),A2(2),x_2x(1),x_2x(2),'m','LineWidth',1) ;
h13 = quiver(A2(1),A2(2),y_2x(1),y_2x(2),'r','LineWidth',1);
h14 = quiver(A3(1),A3(2),x_3x(1),x_3x(2),'m','LineWidth',1) ;
h15 = quiver(A3(1),A3(2),y_3x(1),y_3x(2),'r','LineWidth',1);

x_ax_p = (Rz(thP)*(x_ax)); % platform frame
y_ax_p = (Rz(thP)*(y_ax));

h16 = quiver(pP(1),pP(2),x_ax_p(1),x_ax_p(2),'r--','LineWidth',1) ;
h17 = quiver(pP(1),pP(2),y_ax_p(1),y_ax_p(2),'r--','LineWidth',1);

x_f = [1;0]*0.2*L; % fixed frame
y_f = [0;1]*0.2*L;
quiver(0,0,x_f(1),x_f(2),'b','LineWidth',1) 
quiver(0,0,y_f(1),y_f(2),'b','LineWidth',1)

lim1min = A1 +Rz(th1)*[0;+actlim(1)];
lim1max = A1 +Rz(th1)*[0;+actlim(2)];
line([lim1min(1) lim1max(1)],[lim1min(2) lim1max(2)],'Color','k','LineStyle','--','LineWidth',1)
lim2min = A2 +Rz(th2)*[0;+actlim(1)];
lim2max = A2 +Rz(th2)*[0;+actlim(2)];
line([lim2min(1) lim2max(1)],[lim2min(2) lim2max(2)],'Color','k','LineStyle','--','LineWidth',1)
lim3min = A3 +Rz(th3)*[0;+actlim(1)];
lim3max = A3 +Rz(th3)*[0;+actlim(2)];
line([lim3min(1) lim3max(1)],[lim3min(2) lim3max(2)],'Color','k','LineStyle','--','LineWidth',1)
axis equal

h = [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17];
end
