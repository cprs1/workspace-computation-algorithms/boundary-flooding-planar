function [flag1,flag2] = SingularityMode3PFR(jac,Nf)

A1 = jac(1:3*Nf+3,1:3);
U1 = jac(1:3*Nf+3,1+3:3+3*Nf);
P1 = jac(1:3*Nf+3,1+3+3*Nf:3+3*Nf+3);

A2 = jac(1+3*Nf+3:3*Nf+3+6,1:3);
U2 = jac(1+3*Nf+3:3*Nf+3+6,1+3:3+3*Nf);
P2 = jac(1+3*Nf+3:3*Nf+3+6,1+3+3*Nf:3+3*Nf+3);

G = jac(1:3*Nf+3,1+3+3*Nf+3:3+3+3*Nf+6);
Z = null(G');

T1 = [Z'*A1,Z'*U1;
         A2,   U2];
     
T2 = [Z'*P1,Z'*U1; 
         P2,   U2];

flag1 = rcond(T1);
flag2 = rcond(T2);

end