function flag = StabilityMode3PFR(jac,Nf)

U = jac(1:3*Nf+3,1+3:3+3*Nf);
P = jac(1:3*Nf+3,1+3+3*Nf:3+3*Nf+3);

G = jac(1:3*Nf+3,1+3+3*Nf+3:3+3+3*Nf+6);
Z = null(G');

H = [U,P];
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end