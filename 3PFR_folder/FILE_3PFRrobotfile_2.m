%% 3PFR ROBOT

%% Robot Material Parameters
L = 1;
r = 0.001;
E = 210*10^9;
A = pi*r^2;
I = 0.25*pi*r^4;
rho = 7800;
EI = E*I;

rB = 0.10;   % base radius [m]
rP = 0.15;   % platform radius [m]

th1 = 0; % wrt fixed frame 
th2 = +2*pi/3;
th3 = -2*pi/3;

A1 = rB*[sin(th1);cos(th1)]; % wrt fixed frame 
A2 = rB*[-sin(th2);cos(th2)]; 
A3 = rB*[-sin(th3);cos(th3)];

B1 = rP*[cos(th1);sin(th1)]; % wrt platform frame 
B2 = rP*[cos(th2);sin(th2)]; 
B3 = rP*[cos(th3);sin(th3)];

basepoints = [A1,A2,A3];
platformpoints = [B1,B2,B3];

baseangles = [th1,th2,th3];
geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;
geometry.baseangles = baseangles;

stresslim = +1800*10^6; % Pa

%% External loads

g = [0;0];
fd = rho*A*g;
fend = [0;0];
mend = 0;

wp = [mend;fend];
wd = [0;fd]; % distributed wrench

%% Model Parameters

Nf = 4;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

actlim = [-1,+1];

pstart = [0;0];
th = 0*pi/180;
params.pstart = pstart;

%% Simulation functions
fcn.objetivefcn = @(y,pend) InverseMode3PFR(y,geometry,L,Kad,[pend;th],wp,wd,Nf);
fcn.mechconstrfcn = @(y) mechconstr3PFR(y,actlim,L,r,E,stresslim,Nf);
fcn.singufcn = @(jac) SingularityMode3PFR(jac,Nf);
fcn.stabilityfcn = @(jac) StabilityMode3PFR(jac,Nf);

%% First Initial Guess

qa0 = [0;0;0];
qe0 = zeros(3*Nf,1);
qp0 = [pstart;th];
lambda0 = zeros(6,1);
y0 = [qa0;qe0;qp0;lambda0];

feq = @(y) InverseMode3PFR(y,geometry,L,Kad,[pstart;th],wp,wd,Nf);
options = optimoptions('fsolve','display','iter','Maxiterations',50,'SpecifyObjectiveGradient',true,'CheckGradients',true);

[y,~,solve_flag,~,jac] = fsolve(feq,y0,options);
params.y0 = y;
[pos1,pos2,pos3] = pos3PFR(y,geometry,Nf,L);

Plot3PFR(pos1,pos2,pos3,pstart,th,geometry,L,actlim);

title('Initial Guess Configuration')
drawnow
axis equal
axis([-1 1 -1 1])

