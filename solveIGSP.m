function [y,solve_flag,jac] = solveIGSP(params,fcn,pend,y0)
maxiter = params.maxiter;
options = optimoptions('fsolve','display','off','SpecifyObjectiveGradient',true,'CheckGradients',false,'Maxiter',maxiter);
Cordefun = fcn.objetivefcn;

feq = @(y) Cordefun(y,pend);
[y,~,solve_flag,~,jac] = fsolve(feq,y0,options);

end