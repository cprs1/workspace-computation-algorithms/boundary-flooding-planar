%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to get neighbors of a point

% INPUT:
% WK: array to containt wk points
% point : point in grid to identify neighbors
% params: structure with simulation parameters

% OUTPUT:
% idn: index of neighbors points

function idn = getNeighbors(WK,point,params)

stepsize_x = params.stepsize_x;
stepsize_y = params.stepsize_y;

fact = 1.43;
idn = ((abs(WK(:,2)-point(1))<=fact*stepsize_x) & (abs(WK(:,3)-point(2))<=fact*stepsize_y));
idn = WK((idn==1),1);

end