%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to get uniform grid in the plane

% INPUT:
% params: structure with simulation parameters

% OUTPUT:
% grid: array for points in the grid

function grid = defineGrid(params)
    stepsize_x = params.stepsize_x;
    stepsize_y = params.stepsize_y;
    boxsize = params.boxsize;
    
    xL = boxsize(1);
    xR = boxsize(2);
    yU = boxsize(3);
    yB = boxsize(4);
    n_sampX = floor(abs(xL-xR)/stepsize_x);
    n_sampY = floor(abs(yU-yB)/stepsize_y);
    x = linspace(xL,xR,n_sampX);
    yg = linspace(yB,yU,n_sampY);
    [X,Y] = meshgrid(x,yg);
    Xp = reshape(X,n_sampX*n_sampY,1);
    Yp = reshape(Y,n_sampX*n_sampY,1);
    grid = [Xp,Yp];
end