# Boundary-Flooding-Planar

Algorithm for the computation of workspace boundaries for Continuum Parallel Robots, planar case.

Last update by F. Zaccaria 02 February 2022