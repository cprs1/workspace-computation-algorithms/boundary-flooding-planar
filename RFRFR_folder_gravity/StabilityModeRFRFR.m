function flag = StabilityModeRFRFR(jac,Nf)

U = jac(1:2*Nf+2,1+2:2+2*Nf);
P = jac(1:2*Nf+2,1+2+2*Nf:2+2*Nf+2);

G = jac(1:2*Nf+2,1+2+2*Nf+2:2+2+2*Nf+4);
Z = null(G');

H = [U,P];
Hr = Z'*H*Z;

[~,flag] = chol(Hr);
end