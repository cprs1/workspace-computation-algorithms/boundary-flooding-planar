%% BACKWARD ODEs of a single beam
% for assumed strain mode approach
function yd = OdefunAssumeBackward(s,y,qe,wdl,Nf,L,str)

var = y(1:4+Nf);
ddqa = y(1+4+Nf:4+Nf+4+Nf,1);
ddqe = y(1+4+Nf+4+Nf:4+4+Nf+Nf+4*Nf+Nf*Nf,1);
ddw0 = y(1+4+Nf+4+Nf+(4+Nf)*Nf:4+Nf+4+Nf+(4+Nf)*Nf+(4+Nf)*3,1);
ddth0 = y(1+4+Nf+4+Nf+(4+Nf)*Nf+4*3:4+Nf+4+Nf+(4+Nf)*Nf+4*3+4+Nf,1);

th = var(1);
mz = var(2);
nx = var(3);
ny = var(4);

dthdqa = ddqa(1,1);
dmzdqa = ddqa(2,1);
dnxdqa = ddqa(3,1);
dnydqa = ddqa(4,1);

dthdqe = ddqe(1+0*Nf:1*Nf,1);
dmzdqe = ddqe(1+1*Nf:2*Nf,1);
dnxdqe = ddqe(1+2*Nf:3*Nf,1);
dnydqe = ddqe(1+3*Nf:4*Nf,1);

dthdw0 = ddw0(1+0*3:1*3,1);
dmzdw0 = ddw0(1+1*3:2*3,1);
dnxdw0 = ddw0(1+2*3:3*3,1);
dnydw0 = ddw0(1+3*3:4*3,1);

dthdth0 = ddth0(1,1);
dnxdth0 = ddth0(2,1);
dnydth0 = ddth0(3,1);
dmzdth0 = ddth0(4,1);

b = BaseFcnLegendre(s,1,Nf);
u = b'*qe;
        
%% 

switch str
    case 'fix'

        thd = u;
        dthdqad = 0;
        dthdqed = b;
        dthdw0d = zeros(3,1);
        
        wdf = Rz(th)'*wdl(2:3);
        
        wd = [wdl(1);wdf];
        dwddth0 = [0;Rz(th+pi/2)'*wdl(2:3)*dthdth0];
        dwddqa = [0;Rz(th+pi/2)'*wdl(2:3)*dthdqa];
        dwddqe = [zeros(1,Nf);Rz(th+pi/2)'*wdl(2:3)*dthdqe'];
        dwddw0 = [zeros(1,3);Rz(th+pi/2)'*wdl(2:3)*dthdw0'];
        
        mzd = -ny-wd(1);
        nxd = +u*ny-wd(2);
        nyd = -u*nx-wd(3);
        Qcd = -b*mz;
        
        dmzdqad = -dnydqa ; % wd(1) is constant
        dmzdqed = -dnydqe ; % wd(1) is constant
        dmzdw0d = -dnydw0 ; % wd(1) is constant
        
        dnxdqad = +u*dnydqa -dwddqa(2);
        dnxdqed = +b*ny+u*dnydqe -dwddqe(2,:)';
        dnxdw0d = +u*dnydw0 -dwddw0(2);
        
        dnydqad = -u*dnxdqa -dwddqa(3);
        dnydqed = -b*nx-u*dnxdqe -dwddqe(3,:)';
        dnydw0d = -u*dnxdw0 -dwddw0(3);
        
        dQcdqad = -b*dmzdqa';
        dQcdqed = -b*dmzdqe';
        dQcdw0d = -b*dmzdw0';
        
        dthdth0d = 0;
        dnxdth0d = +u*dnydth0-dwddth0(1);
        dnydth0d = -u*dnxdth0-dwddth0(2);
        dmzdth0d = -dnydth0-dwddth0(3);
        dQcdth0d = -b*dmzdth0;
        
        yd = L*[thd;mzd;nxd;nyd;Qcd;
                dthdqad;dmzdqad;dnxdqad;dnydqad;dQcdqad;
                dthdqed;dmzdqed;dnxdqed;dnydqed;reshape(dQcdqed,Nf*Nf,1);
                dthdw0d;dmzdw0d;dnxdw0d;dnydw0d;reshape(dQcdw0d,3*Nf,1);
                dthdth0d;dnxdth0d;dnydth0d;dmzdth0d;dQcdth0d;];
    case 'variable'
        
        thd = L*u;
        dthdqad = u;
        dthdqed = L*b;
        dthdw0d = zeros(3,1);
        
        wdf = Rz(th)'*wdl(2:3);
        
        wd = [wdl(1);wdf];
        dwddth0 = [0;Rz(th+pi/2)'*wdl(2:3)*dthdth0];
        dwddqa = [0;Rz(th+pi/2)'*wdl(2:3)*dthdqa];
        dwddqe = [zeros(1,Nf);Rz(th+pi/2)'*wdl(2:3)*dthdqe'];
        dwddw0 = [zeros(1,3);Rz(th+pi/2)'*wdl(2:3)*dthdw0'];
        
        mzd = -L*ny-L*wd(1);
        nxd = +L*u*ny-L*wd(2);
        nyd = -L*u*nx-L*wd(3);
        Qcd = -L*b*mz;
        
        dmzdqad = -L*dnydqa - ny -wd(1); % wd(1) is constant
        dmzdqed = -L*dnydqe ; % wd(1) is constant
        dmzdw0d = -L*dnydw0 ; % wd(1) is constant
        
        dnxdqad = +L*u*dnydqa -L*dwddqa(2) +u*ny - wd(2);
        dnxdqed = +L*b*ny+u*dnydqe -L*dwddqe(2,:)';
        dnxdw0d = +L*u*dnydw0 -L*dwddw0(2);
        
        dnydqad = -L*u*dnxdqa -L*dwddqa(3) -u*nx-wd(3);
        dnydqed = -L*b*nx-u*dnxdqe -L*dwddqe(3,:)';
        dnydw0d = -L*u*dnxdw0 -L*dwddw0(3);
        
        dQcdqad = -L*b*dmzdqa' - b'*mz;
        dQcdqed = -L*b*dmzdqe';
        dQcdw0d = -L*b*dmzdw0';
        
        dthdth0d = 0;
        dnxdth0d = +L*u*dnydth0-dwddth0(1);
        dnydth0d = -L*u*dnxdth0-dwddth0(2);
        dmzdth0d = -L*dnydth0-dwddth0(3);
        dQcdth0d = -L*b*dmzdth0;
        
         yd = [thd;mzd;nxd;nyd;Qcd;
                dthdqad;dmzdqad;dnxdqad;dnydqad;dQcdqad;
                dthdqed;dmzdqed;dnxdqed;dnydqed;reshape(dQcdqed,Nf*Nf,1);
                dthdw0d;dmzdw0d;dnxdw0d;dnydw0d;reshape(dQcdw0d,3*Nf,1);
                dthdth0d;dnxdth0d;dnydth0d;dmzdth0d;dQcdth0d;];
    otherwise
        error('Define settings for beams length');
end

end