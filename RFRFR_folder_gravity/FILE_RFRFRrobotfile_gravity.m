%% Robot Geometry Parameters
% close all

rAB = 0.4;
pA1 = [-rAB/2;0];
pA2 = [+rAB/2;0];
geometry.basepoints = [pA1,pA2];

%% Robot Material Parameters
L = 1;
d = 0.002;
A = pi*d^2/4;
Ix = pi*d^4/64;
E = 210*10^9;
rho = 7800;
EI = E*Ix;

stresslim = 1860*10^6;%E*1; % Pa
torquelim = Inf; %Nm
%% External loads

g = [0;0];
fd = rho*A*g;
fend = [0;0];


%% Model Parameters

Nf = 6;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

th1lim = [0,2*pi];
th2lim = [0,2*pi];

pstart = [0;0.8];
params.pstart = pstart;

%% Simulation functions
fcn.objetivefcn = @(y,pend) InverseModeForwBackRFRFSeqn_mex(y,geometry,Kad,L,fend,[0;fd],pend,Nf);
fcn.mechconstrfcn = @(y) mechconstrRFRFR(y,th1lim,th2lim,L,d/2,E,stresslim,torquelim,Nf,geometry,[0;fd]);
fcn.singufcn = @(jac) SingularityModeRFRFR(jac,Nf);
fcn.stabilityfcn = @(jac) StabilityModeRFRFR(jac,Nf);
fcn.internfcn = @(y) InternalEnergy(y,Kad,L,Nf);
%% First Initial Guess

qa0 = [pi/2;pi/2];
qe0 = [-.1;zeros(Nf-1,1);+.1;zeros(Nf-1,1)];
qp0 = pstart;
lambda0 = 0.0001*rand(4,1);
y0 = [qa0;qe0;qp0;lambda0];

feq = @(y) InverseModeForwBackRFRFSeqn(y,geometry,Kad,L,fend,[0;fd],pstart,Nf);
options = optimoptions('fsolve','display','iter','SpecifyObjectiveGradient',true,'CheckGradients',true,'Algorithm','trust-region');

[y,~,solve_flag,~,jac] = fsolve(feq,y0,options);
params.y0 = y;
[pos1,pos2] = posRFRFRmode(y,L,[pA1,pA2],Nf,100);

PlotRFRFR(pos1,pos2,L);
title('Initial Guess Configuration')
drawnow
axis equal
axis([-1 1 -1 1])

drawnow