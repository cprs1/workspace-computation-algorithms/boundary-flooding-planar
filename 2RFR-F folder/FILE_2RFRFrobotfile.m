%% 2RFR-F ROBOT
%% GEOMETRICAL PARAMETERS

% base
rAB = 0.5;
A1 = [-rAB/2;0];
A2 = [0;+.10];
A3 = [+rAB/2;0];
basepoints = [A1,A2,A3];

% platform
lP = 0.3;
B1 = [-lP/2;0];
B2 = [0;+0.1];
B3 = [+lP/2;0];
platpoints = [B1,B2,B3];

rodbaseangle = +pi/2;

geometry.basepoints = basepoints;
geometry.platpoints = platpoints;
geometry.rodbaseangle = rodbaseangle;

%% MATERIAL PARAMETERS
r = 0.002;
% E = 210*10^9;
E = 25*10^9;
I = 0.25*pi*r^4;
L = 1; % for external beams
EI = E*I;
stresslim = +400*10^6;

%% EXTERNAL LOADS
fd = [0;0];
fend = [0;0];
mend = 0;
wp = [mend;fend];
wd = [0;fend];

%% Model Parameters

Nf = 4;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

actlim = [0.1,+Inf];

pstart = [0.3;0.7];
th = -70*pi/180;
params.pstart = pstart;

%% Simulation functions
fcn.objetivefcn = @(y,pend) InverseMode2_RFR_F(y,geometry,L,Kad,[pend;th],wp,wd,Nf);
fcn.mechconstrfcn = @(y) mechconstr2RFRF(y,actlim,r,E,stresslim,Nf);
fcn.singufcn = @(jac) SingularityMode2RFRF(jac,Nf);
fcn.stabilityfcn = @(jac) StabilityMode2RFRF(jac,Nf);

%% First Initial Guess

qa0 = [150*pi/180; 0.7; 30*pi/180];
qe0 = zeros(3*Nf,1);
qp0 = [pstart;th];
lambda0 = 0.00001*rand(7,1);
y0 = [qa0;qe0;qp0;lambda0];

feq = @(y) InverseMode2_RFR_F(y,geometry,L,Kad,[pstart;th],wp,wd,Nf);
options = optimoptions('fsolve','display','iter','Maxiterations',50,'SpecifyObjectiveGradient',true,'CheckGradients',true,'Algorithm','trust-region');

[y,~,solve_flag,~,jac] = fsolve(feq,y0,options);
params.y0 = y;

[pos1,pos2,pos3] = pos2_RFR_F(y,geometry,Nf,L);
Plot2_RFR_F(pos1,pos2,pos3,pstart,th,geometry,L);

title('Initial Guess Configuration')
drawnow
axis equal
axis([-1 1 -1 1])

