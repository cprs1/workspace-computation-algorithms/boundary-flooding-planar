function h = Plot2_RFR_F(pos1,pos2,pos3,pP,thP,geometry,L)
basepoints = geometry.basepoints;
platformpoints = geometry.platpoints;

A1 = basepoints(:,1);
A2 = basepoints(:,2);
A3 = basepoints(:,3);
B1 = platformpoints(:,1);
B2 = platformpoints(:,2);
B3 = platformpoints(:,3);

B1f = Rz(thP)*B1+pP; %wrt fixed frame
B2f = Rz(thP)*B2+pP;
B3f = Rz(thP)*B3+pP;

plot(A1(1),A1(2),'bo','LineWidth',1) % base points
hold on
plot(A2(1),A2(2),'bs','LineWidth',1)
plot(A3(1),A3(2),'bo','LineWidth',1)
grid on
h1 = plot(B1f(1),B1f(2),'ko','LineWidth',1); % platform points
h2 = plot(B2f(1),B2f(2),'ks','LineWidth',1);
h3 = plot(B3f(1),B3f(2),'ko','LineWidth',1);

platwidth = norm(B3f-B1f);
B2p = B2f+Rz(thP)*[platwidth/3;0];
B2m = B2f-Rz(thP)*[platwidth/3;0];

% line([B1f(1) B2f(1)],[B1f(2) B2f(2)],'Color','k','LineWidth',1) % platform
% line([B2f(1) B3f(1)],[B2f(2) B3f(2)],'Color','k','LineWidth',1)
% line([B3f(1) B1f(1)],[B3f(2) B1f(2)],'Color','k','LineWidth',1)
h4 = line([B1f(1) B3f(1)],[B1f(2) B3f(2)],'Color','k','LineWidth',1); % platform
h5 = line([B2p(1) B3f(1)],[B2p(2) B3f(2)],'Color','k','LineWidth',1);
h6 = line([B2m(1) B1f(1)],[B2m(2) B1f(2)],'Color','k','LineWidth',1);
h7 = line([B2m(1) B2p(1)],[B2m(2) B2p(2)],'Color','k','LineWidth',1);
h8 = plot(pos1(1,:),pos1(2,:),'k','LineWidth',1); % flexible beams
h9 = plot(pos2(1,:),pos2(2,:),'k','LineWidth',1);
h10 = plot(pos3(1,:),pos3(2,:),'k','LineWidth',1);

x_ax = [1;0]*0.2*L; % fixed frame
y_ax = [0;1]*0.2*L;

quiver(0,0,x_ax(1),x_ax(2),'b','LineWidth',1) 
quiver(0,0,y_ax(1),y_ax(2),'b','LineWidth',1)

x_ax_p = Rz(thP)*x_ax; % platform frame
y_ax_p = Rz(thP)*y_ax;

h11 = quiver(pP(1),pP(2),x_ax_p(1),x_ax_p(2),'r--','LineWidth',1) ;
h12 = quiver(pP(1),pP(2),y_ax_p(1),y_ax_p(2),'r--','LineWidth',1);
axis equal
axis(1.1*[-1 1 -1 1])

h = [h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12];
end