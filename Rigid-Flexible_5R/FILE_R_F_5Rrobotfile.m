%% Robot Geometry Parameters
rAB = 0.2;
ang_offset = pi/2;
pA1 = [-rAB/2;0];
pA2 = [+rAB/2;0];
geometry.basepoints = [pA1,pA2];
geometry.angoffset = [+ang_offset;-ang_offset];

%% Robot Material Parameters
r = 0.001;
E = 210*10^9;
Lf = .7;
Lr = .3;
A = pi*r^2;
I = 0.25*pi*r^4;
EI = E*I;
Kbt = E*I;
g = [0;0];
rho = 7800;
f = rho*A*g;
fext = [0;.0];

wp = fext; % RFRFR no moments on EE
wd = [0;f]; % distributed wrench
stresslim = Inf;

%% External loads

g = [0;0];
fd = rho*A*g;
fend = [0;0];


%% Model Parameters

Nf = 4;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

th1lim = [0,2*pi];
th2lim = [0,2*pi];

pstart = [0;0.85];
params.pstart = pstart;

%% Simulation functions
fcn.objetivefcn = @(y,pend) InverseModeR_F_5R(y,geometry,Kad,Lf,Lr,wp,wd,pend,Nf);
fcn.mechconstrfcn = @(y) mechconstrR_F_5R(y,th1lim,th2lim,Lf,r,E,stresslim,Nf);
fcn.singufcn = @(jac) SingularityModeR_F_5R(jac,Nf);
fcn.stabilityfcn = @(jac) StabilityModeR_F_5R(jac,Nf);

%% First Initial Guess

qa0 = [+150;+30]*pi/180;
qe0 = 0.001*zeros(2*Nf,1);
qp0 = pstart;
lambda0 = zeros(4,1);
y0 = [qa0;qe0;qp0;lambda0];

feq = @(y) InverseModeR_F_5R(y,geometry,Kad,Lf,Lr,wp,wd,pstart,Nf);
options = optimoptions('fsolve','display','iter','SpecifyObjectiveGradient',true,'CheckGradients',true);

[y,~,solve_flag,~,jac] = fsolve(feq,y0,options);
params.y0 = y;

[pos1,pos2] = poseR_F_5R(y,geometry,Lf,Lr,Nf);

h = PlotR_F_5R(pos1,pos2,geometry);
title('Initial Guess Configuration')
drawnow
axis equal
axis([-1 1 -1 1])

