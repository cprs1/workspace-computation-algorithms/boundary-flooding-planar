function h = PlotR_F_5R(pos1,pos2,geometry)
% figure()
basepoints = geometry.basepoints;
A1 = basepoints(:,1);
A2 = basepoints(:,2);

h1 = plot(pos1(1,:),pos1(2,:),'k-','LineWidth',1.5); % beam 1
hold on
h2 = plot(pos2(1,:),pos2(2,:),'k-','LineWidth',1.5); % beam 2
h3 = plot(pos2(1,end),pos2(2,end),'ko','LineWidth',2); % EE

h4 = plot(pos1(1,1),pos1(2,1),'ks','LineWidth',2); % anchor point 2
h5 = plot(pos2(1,1),pos2(2,1),'ks','LineWidth',2); % anchor point 2

h6 = plot(A1(1,1),A1(2,1),'ro','LineWidth',2); % base point 1
plot(A2(1,1),A2(2,1),'ro','LineWidth',2); % base point 2

h7 = line([A1(1),pos1(1,1)],[A1(2),pos1(2,1)],'Color','Blue','LineWidth',2);
h8 = line([A2(1),pos2(1,1)],[A2(2),pos2(2,1)],'Color','Blue','LineWidth',2);
quiver(0,0,1/5,0,'b','LineWidth',2) % base frame
quiver(0,0,0,1/5,'b','Linewidth',2)
axis equal
axis([-1 1 -1 1])
grid on

h = [h1;h2;h3;h4;h5;h6;h7;h8];
end