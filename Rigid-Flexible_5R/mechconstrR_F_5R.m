function flag = mechconstrR_F_5R(y,th1lim,th2lim,Lf,r,E,stresslim,Nf)

qA1 = wrapTo2Pi(y(1,1));
qA2 = wrapTo2Pi(y(2,1));
qe1 = y(1+2:2+Nf,1);
qe2 = y(1+2+Nf:2+2*Nf,1);


flag1 = (th1lim(1)<=qA1 & qA1<=th1lim(2));      % motor constraint 1
flag2 = (th2lim(1)<=qA2 & qA2<=th2lim(2));  	% motor constraint 2

Nsamp = 100; % sample in 100 points
s = linspace(0,Lf,Nsamp);
b = BaseFcnLegendre(s,Lf,Nf);

strain1 = b'*qe1;
strain2 = b'*qe2;
sigma1 = r*E*strain1;
sigma2 = r*E*strain2;
s1 = max(((sigma1).^2.).^0.5);
s2 = max(((sigma2).^2.).^0.5);
stress = max(s1,s2);
flag4 = stress<=stresslim;
 
flag = (flag1 & flag2 & flag4);

   
end