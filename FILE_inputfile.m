%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

%% INPUT FILE

%% Simulation parameters
% to obtain fixed grid behaviour, set stepsize = stepmin
guessflag = 'C'; %choose between K (bestkantorovich) or C (bestconditioning)

maxiter = 20;
TOL = 10^-4;
TOL2 = 800 ;
stepsize = 0.02;
boxsize = [-1 1 -1 1];

n_rad = 8;
tau = 800;

%% STORE VARIABLES

params.n_rad = n_rad;
params.stepsize_x = stepsize;
params.stepsize_y = stepsize;
params.maxiter = maxiter;
params.boxsize = boxsize;
params.TOL = TOL;
params.TOL2 = TOL2;
params.guessflag = guessflag;
params.tau = tau;

