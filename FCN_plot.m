%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to get plots

% INPUT:
% WK: array to containt wk points
% parameters: structure with simulation parameters
% n_rad: number of radiating directions

function areatot = FCN_plot(WK,parameters,n_rad)

boxsize = parameters.boxsize;

%% NUMERICAL RESULT
figure()

idwk = (WK(:,4) == 1  & WK(:,5) ~=0);
idT1 = (WK(:,4) == 2) | (WK(:,4) == 3) | (WK(:,4) == 4);
idT2 = (WK(:,4) == 5) | (WK(:,4) == 6);
idmech = (WK(:,4) == 7);
idrad_vector = (WK(:,4) == 1 & WK(:,5) ==0);

plot(WK(idwk,2),WK(idwk,3),'b.')
hold on
plot(WK(idrad_vector,2),WK(idrad_vector,3),'c.')
plot(WK(idT1,2),WK(idT1,3),'r.')
plot(WK(idT2,2),WK(idT2,3),'k.')
plot(WK(idmech,2),WK(idmech,3),'g.')




xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
axis equal
axis(boxsize)
title('Workspace Computation')

%% COMPUTE ALL BOUNDARIES
areap = zeros(n_rad,1);
polyshapes = cell(n_rad,1);
figure()
hold on
for i = 1:n_rad
    idwk_i = (WK(:,4) ~= 1 & WK(:,5) == i );
    if numel(WK(idwk_i==1,1))>0
        points = [WK(idwk_i,2),WK(idwk_i,3)];
       [k,areap(i)] = boundary(points(:,1),points(:,2),.8);
       if numel(k)>1
           polyshapes{i,1} = polyshape(points(k,1),points(k,2));
           plot(polyshapes{i})
       end
    end
end
xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
axis equal
axis(boxsize)
title('Workspace Boundary')

%% COMPUTE FIGURE
maxarea = max(areap);

[~,I] = sort(areap);

areap = flip(areap(I));
polyshapes = flip(polyshapes(I));
figure()
hold on
for i = 1:n_rad
    if areap(i)>=0.02*maxarea && areap(i)>0
        if areap(i)==maxarea
            h = polyshapes{i};
            plot(h,'FaceColor','Blue','FaceAlpha',1,'LineWidth',2);
        else
            h = polyshapes{i};
            plot(h,'FaceColor','White','FaceAlpha',1,'LineWidth',2);
        end
    end
end
grid on
axis equal
axis(boxsize)
    %% COMPUTE TOTAL WORKSPACE AREA
areap = flip(sort(areap));
if numel(areap)>1
    areatot = areap(1)-sum(areap(2:end));
else
    areatot = areap;
end

%% REFINED FIGURE FOR PAPER

figure()
hold on
for i = 1:n_rad
    if areap(i)>=0.02*maxarea && areap(i)>0
        if areap(i)==maxarea
            h = polyshapes{i};
            plot(h,'FaceColor','Blue','FaceAlpha',.5,'LineWidth',0.1);
        else
            h = polyshapes{i};
            plot(h,'FaceColor','White','FaceAlpha',1,'LineWidth',0.1);
        end
    end
end
hold on
plot(WK(idwk,2),WK(idwk,3),'b.')
plot(WK(idrad_vector,2),WK(idrad_vector,3),'c.')
plot(WK(idT1,2),WK(idT1,3),'r.')
plot(WK(idT2,2),WK(idT2,3),'k.')
plot(WK(idmech,2),WK(idmech,3),'g.')
 axis equal
axis([-1.1 1.1 -1.1 1.1])
end