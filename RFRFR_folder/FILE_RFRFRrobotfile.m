%% Robot Geometry Parameters
rAB = 0.4;
pA1 = [-rAB/2;0];
pA2 = [+rAB/2;0];
geometry.basepoints = [pA1,pA2];

%% Robot Material Parameters
L = 1;
r = 0.001;
E = 210*10^9;
A = pi*r^2;
I = 0.25*pi*r^4;
rho = 7800;
EI = E*I;

stresslim = 0.8*E; % Pa

%% External loads

g = [0;0];
fd = rho*A*g;
fend = [0;0];


%% Model Parameters

Nf = 6;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

th1lim = [0,2*pi];
th2lim = [0,2*pi];

pstart = [-0.10;-0.85];
params.pstart = pstart;

%% Simulation functions
fcn.objetivefcn = @(y,pend) InverseModeForwBackRFRFSeqn(y,geometry,Kad,L,fend,fd,pend,Nf);
fcn.mechconstrfcn = @(y) mechconstrRFRFR(y,th1lim,th2lim,L,r,E,stresslim,Nf);
fcn.singufcn = @(jac) SingularityModeRFRFR(jac,Nf);
fcn.stabilityfcn = @(jac) StabilityModeRFRFR(jac,Nf);

%% First Initial Guess

qa0 = [-pi/2;-pi/2];
qe0 = [-.2;zeros(Nf-1,1);+.2;zeros(Nf-1,1)];
qp0 = pstart;
lambda0 = zeros(4,1);
y0 = [qa0;qe0;qp0;lambda0];

feq = @(y) InverseModeForwBackRFRFSeqn(y,geometry,Kad,L,fend,fd,pstart,Nf);
options = optimoptions('fsolve','display','iter','SpecifyObjectiveGradient',true,'CheckGradients',true);

[y,~,solve_flag,~,jac] = fsolve(feq,y0,options);
params.y0 = y;
[pos1,pos2] = posRFRFRmode(y,L,[pA1,pA2],Nf);

PlotRFRFR(pos1,pos2,L);
title('Initial Guess Configuration')
drawnow
axis equal
axis([-1 1 -1 1])

