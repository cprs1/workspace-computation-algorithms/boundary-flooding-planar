function [pos1,pos2] = posRFRFRmode(sol,L,basepoints,Nf)

Nsh = 100;
s_span = linspace(0,L,Nsh);
posbeams = zeros(4,Nsh);
qe =  sol(1+2:2+2*Nf,1);
qa = sol(1:2);

Lspan = [0,1];

for i = 1:2
    qai = qa(i);
    qei = qe(1+Nf*(i-1):Nf*i,1);
    fun = @(s,y) OdeFunReconstruct(s,y,qei,Nf,L);
    p0 = basepoints(:,i);
    th0 = qai;
    y0 = [p0;th0];
    [si,yi] = ode45(fun,Lspan,y0);
    posbeams(1+2*(i-1):2*i,:) = spline(L*si,yi(:,1:2)',s_span);
end

pos1 = posbeams(1:2,:);
pos2 = posbeams(3:4,:);

end