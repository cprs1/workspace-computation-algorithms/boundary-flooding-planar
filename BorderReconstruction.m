function [WK,guesses] = BorderReconstruction(wk_todo,WK,guesses,params,fcn,nr,drawnowopts)

while numel(wk_todo)>0
    % EXTRACT POINT to do
    fail_idx = wk_todo(1,1);
    workspace_idx = wk_todo(1,2);
    wk_todo = wk_todo(2:end,:);
    p_fail = [WK(fail_idx,2),WK(fail_idx,3)];
    pstart = [WK(workspace_idx,2),WK(workspace_idx,3)];
    %create box around fail and sort them
    [id_neigh_todo,idn2] = getFailBox(WK,p_fail,pstart,fail_idx,params);
    while numel(id_neigh_todo)>0
        % find the first to do
        current_idx = id_neigh_todo(1,1);
        pend = [WK(current_idx,2);WK(current_idx,3)];
        id_neigh_todo = id_neigh_todo(2:end,1);
        % find its neighbors in wk in idn2
        idx_wk =(WK(idn2,4)==1);
        idm_t = idn2(idx_wk==1);
        idm = getNeighbors(WK(idm_t,:),pend,params);
        if numel(idm)~=0
            % compute
            [~,idmin] = min(WK(idm,6));
            id_start = idm(idmin);
            y0 = guesses(:,id_start);
            [y,solve_flag,jac] = solveIGSP(params,fcn,pend,y0);
            % border-conditions
            solve_flag = ((solve_flag == 1) || (solve_flag == 2) || (solve_flag == 3) || (solve_flag == 4));
            norminv = norm(inv(jac),Inf);
            WK(current_idx,6) = norminv;
            WK(current_idx,5) = nr;
            [exitflag,exitidx] = getOutCondition(y,jac,pend,solve_flag,norminv,params,fcn);
            WK(current_idx,4) = exitidx;
            if exitflag == 1
                WK(current_idx,5) = nr;
                guesses(:,current_idx) = y;
                if drawnowopts == 1
                    plot(pend(1),pend(2),'b.')
                    drawnow
                end
            else
                new_todo = [current_idx,id_start];
                wk_todo = [wk_todo;new_todo];
                if drawnowopts == 1
                    plot(pend(1),pend(2),'r.')
                    drawnow
                end
            end
        end
    end
end
end