%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% Main file

%% MAIN FILE
clear
close all
clc

%% ROBOT PARAMETERS
% FILE_3RFRrobotfile
% FILE_RFRFRrobotfile_gravity
FILE_RFRFRrobotfile
% FILE_3PFRrobotfile
% FILE_3PFRrobotfile_2
% FILE_2RFRFrobotfile
% FILE_R_F_5Rrobotfile

%% SIMULATION PARAMETERS
FILE_inputfile

%%
instantplot = 1; % set 1 to see how it works during computation
[WK,outstruct] = FCN_BoundaryFloodingPlanarWK(fcn,params,instantplot);

%%

areatot = FCN_plot(WK,params,n_rad)
