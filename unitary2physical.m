%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to convert points in unitary space to points in physical space
% INPUT:
% pu: point in unitary space
% params: structure with simulation parameters

% OUTPUT: 
% pf: point in physical space

function pf = unitary2physical(pu,params)
boxsize = params.boxsize;

xmin = boxsize(1);
xmax = boxsize(2);
ymin = boxsize(3);
ymax = boxsize(4);
    
pf = [xmin + pu(1)*(xmax-xmin);
      ymin + pu(2)*(ymax-ymin);];
end