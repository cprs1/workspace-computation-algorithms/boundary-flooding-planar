function flag = mechconstr3RFR(y,th1lim,th2lim,th3lim,L,r,E,stresslim,Nf)

qA1 = wrapTo2Pi(y(1,1));
qA2 = wrapTo2Pi(y(2,1));
qA3 = wrapTo2Pi(y(3,1));

qe1 = y(1+3:3+Nf,1);
qe2 = y(1+3+Nf:3+2*Nf,1);
qe3 = y(1+3+2*Nf:3+3*Nf,1);

flag1 = (th1lim(1)<=qA1 & qA1<=th1lim(2));      % motor constraint 1
flag2 = (th2lim(1)<=qA2 & qA2<=th2lim(2));  	% motor constraint 2
flag3 = (th3lim(1)<=qA3 & qA3<=th3lim(2));  	% motor constraint 3

Nsamp = 100; % sample in 100 points
s = linspace(0,L,Nsamp);
b = BaseFcnLegendre(s,L,Nf);

strain1 = b'*qe1;
strain2 = b'*qe2;
strain3 = b'*qe3;
sigma1 = r*E*strain1;
sigma2 = r*E*strain2;
sigma3 = r*E*strain3;
s1 = max(((sigma1).^2.).^0.5);
s2 = max(((sigma2).^2.).^0.5);
s3 = max(((sigma3).^2.).^0.5);
stress = max([s1,s2,s3]);
flag4 = stress<=stresslim;
 
flag = (flag1 & flag2 & flag3 & flag4);

   
end