function [WK,guesses,start_to_go_idx] = initialization(params,fcn)

y0 = params.y0;
pinit = params.pstart;
Cordefun = fcn.objetivefcn;
maxiter = params.maxiter;

table = defineGrid(params);
np = numel(table(:,1));
idx = 1:1:np;
WK = [idx',table,zeros(np,3)];
guesses = zeros(max(size(y0)),np);


%% INITIALIZATION

start_to_go_idx = getNeighbors(WK,pinit,params);
start_to_go_idx = start_to_go_idx(1);

pend = [WK(start_to_go_idx,2);WK(start_to_go_idx,3)];

feq = @(y) Cordefun(y,pend);
options = optimoptions('fsolve','display','off','SpecifyObjectiveGradient',true,'CheckGradients',false,'Maxiter',maxiter);
[y_start,~,~,~,~] = fsolve(feq,y0,options);

% store results
WK(start_to_go_idx,4) = 1; % in WK
guesses(:,start_to_go_idx) = y_start; % store for future

end