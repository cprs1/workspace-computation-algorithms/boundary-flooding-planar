%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to get attractive points in WK

% INPUT:
% params: structure with simulation parameters

% OUTPUT:
% attractive_points: coordinate of attractive points in unitary space

function attractive_points = getAttractiveCooord(params)
% get attractive_points in unitary domain
n_rad = params.n_rad;
pvect = linspace(0,1,n_rad)';
nvect = flip(pvect);
lato_1 = [pvect,zeros(n_rad,1)];
lato_2 = [ones(n_rad-1,1),pvect(2:end)];
lato_3 = [nvect(2:end),ones(n_rad-1,1)];
lato_4 = [zeros(n_rad-2,1),nvect(2:end-1)];
attractive_coord = [lato_1;lato_2;lato_3;lato_4];

idx = randperm(max(size(attractive_coord)));

attractive_points = attractive_coord(idx(1:n_rad),:);
end